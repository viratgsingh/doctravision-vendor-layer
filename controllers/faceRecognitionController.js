/* eslint-disable max-statements */
/* eslint-disable multiline-comment-style */
const { indexFaceSchema, collectionIdSchema, searchFacesSchema, detectFacesSchema, listFacesSchema } = require("../helpers/validationSchema");
const HttpStatus = require('http-status-codes');
const { RESPONSE_STATUS } = require('../config/constants');
const faceRecognitionBusinessLayer = require('../services/faceRecognitionBusinessLayer');

const indexFace = async (req, res) => {
    var requestData = req.body;
    if (req.files) {
        requestData.image = {};
        requestData.image.mimetype = req.files.image.mimetype;
        requestData.image.size = req.files.image.size;
    }
    const validationResult = indexFaceSchema.validate(requestData);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await faceRecognitionBusinessLayer.indexFace(req);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const detectFaces = async (req, res) => {
    var requestData = req.body;
    if (req.files) {
        requestData.image = {};
        requestData.image.mimetype = req.files.image.mimetype;
        requestData.image.size = req.files.image.size;
    }
    const validationResult = detectFacesSchema.validate(requestData);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await faceRecognitionBusinessLayer.detectFaces(req);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const createCollection = async (req, res) => {
    const validationResult = collectionIdSchema.validate(req.query);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await faceRecognitionBusinessLayer.createCollection(req.query.collectionId);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const deleteCollection = async (req, res) => {
    const validationResult = collectionIdSchema.validate(req.query);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await faceRecognitionBusinessLayer.deleteCollection(req.query.collectionId);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const searchFaces = async (req, res) => {
    var requestData = req.body;
    if (req.files) {
        requestData.image = {};
        requestData.image.mimetype = req.files.image.mimetype;
        requestData.image.size = req.files.image.size;
    }
    const validationResult = searchFacesSchema.validate(requestData);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await faceRecognitionBusinessLayer.searchFaces(req);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const listFaces = async (req, res) => {
    const validationResult = listFacesSchema.validate(req.body);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await faceRecognitionBusinessLayer.listFaces(req);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

module.exports = {
    indexFace,
    createCollection,
    deleteCollection,
    searchFaces,
    detectFaces,
    listFaces
};
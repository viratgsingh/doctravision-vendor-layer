/* eslint-disable multiline-comment-style */
/* eslint-disable max-statements */
const { fileUploadSchema, fileGetSchema, fileDownloadSchema, listFilesSchema } = require("../helpers/validationSchema");
const HttpStatus = require('http-status-codes');
const { RESPONSE_STATUS } = require('../config/constants');
const awsS3BusinessLayer = require('../services/fileBusinessLayer');

const uploadFile = async (req, res) => {
    var requestData = req.body;
    if (req.files && req.files.file) {
        requestData.file = {};
        requestData.file.mimetype = req.files.file.mimetype;
        requestData.file.size = req.files.file.size;
    }
    const validationResult = fileUploadSchema.validate(requestData);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await awsS3BusinessLayer.uploadFile(req);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const getFile = async (req, res) => {
    var params = {
        file: req.params.file + req.params[0]
    };
    const validationResult = fileGetSchema.validate(params);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await awsS3BusinessLayer.getFile(params.file);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.setHeader('Content-Type', data.ContentType);
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(data.Body);
        }
    }
};

const listFiles = async (req, res) => {
    var params = {
        folder: req.params.folder + req.params[0]
    };
    const validationResult = listFilesSchema.validate(params);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await awsS3BusinessLayer.listFiles(params.folder);
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data
            });
        }
    }
};

const downloadFiles = async (req, res) => {
    const validationResult = fileDownloadSchema.validate(req.body);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var data = await awsS3BusinessLayer.downloadFiles(req.body.files);
        var folder = 'signatures';
        if (req.body.folder) {
            folder = req.body.folder;
        }
        if (data.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: data.error
            });
        } else {
            res.setHeader("Content-Type", "application/zip");
            res.setHeader("Content-Disposition", `attachment; filename=${folder}.zip`);
            var bufferData = data.toBuffer();
            res.setHeader('Content-Length', bufferData.length);
            res.send({
                status: RESPONSE_STATUS.STATUS_SUCCESS,
                data: bufferData
            });
        }
    }
};

module.exports = {
    uploadFile,
    getFile,
    downloadFiles,
    listFiles
};
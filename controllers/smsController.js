const { sendSMSSchema } = require("../helpers/validationSchema");
const HttpStatus = require('http-status-codes');
const { RESPONSE_STATUS } = require('../config/constants');
const smsBusinessLayer = require('../services/smsBusinessLayer');

const sendOTP = async (req, res) => {
    const validationResult = sendSMSSchema.validate(req.body);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var response = await smsBusinessLayer.sendSMS(req.body.countryCode, req.body.phone, req.body.message);
        if (response.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: response.error
            });
        } else {
            res.send(response.data);
        }
    }
};

module.exports = {
    sendOTP
};
const { sendEmailSchema } = require("../helpers/validationSchema");
const HttpStatus = require('http-status-codes');
const { RESPONSE_STATUS } = require('../config/constants');
const emailBusinessLayer = require('../services/emailBusinessLayer');

const sendEmail = async (req, res) => {
    const validationResult = sendEmailSchema.validate(req.body);
    if (validationResult.error) {
        res.send({
            status: RESPONSE_STATUS.STATUS_FAIL,
            error: { errorCode: HttpStatus.StatusCodes.BAD_REQUEST, message: validationResult.error.message }
        });
    } else {
        var response = await emailBusinessLayer.sendEmail(req.body.email, req.body.subject, req.body.body, req.body.isHtml);
        if (response.error) {
            res.send({
                status: RESPONSE_STATUS.STATUS_FAIL,
                error: response.error
            });
        } else {
            res.send(response.data);
        }
    }
};

module.exports = {
    sendEmail
};
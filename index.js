require('dotenv').config({path:__dirname + '/.env.' + `${process.env.NODE_ENV}`});
const express = require('express');
const expressLogging = require('express-logging');
const logger = require('logops');
const FileUpload = require('express-fileupload');
var emailRoute = require('./routes/emailRoute');
var smsRoute = require('./routes/smsRoute');
var uploadRoute = require('./routes/fileRoute');
var faceRocognitionRoute = require('./routes/faceRocognitionRoute');
const { genericErrorHandler, unknownRoutesHandler } = require('./handlers/error/errorHandler');

var app = express();
//Logging
app.use(expressLogging(logger, { policy: 'params' }));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(FileUpload());

app.use('/vendor/email', emailRoute);
app.use('/vendor/sms', smsRoute);
app.use('/vendor/file', uploadRoute);
app.use('/vendor/face', faceRocognitionRoute);

// this matches all routes and all methods
app.use(unknownRoutesHandler);
//Custom error handler. Always define at last
app.use(genericErrorHandler);

let port = process.env.NODE_PORT;
app.listen(port);
console.log(`Server started at port ${port}`);
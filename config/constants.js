module.exports = {
    FILE_UPLOAD_CONFIG: {
        IMAGE: {
            MIME_TYPES: ['image/png', 'image/jpg', 'image/jpeg'],
            LIMIT: 5 * 1000 * 1000
        },
        FORM: {
            MIME_TYPES: ['application/pdf'],
            LIMIT: 15 * 1000 * 1000
        },
        VIDEO: {
            MIME_TYPES: ['video/mp4'],
            LIMIT: 30 * 1000 * 1000
        },
        ALL: {
            MIME_TYPES: ['image/png', 'image/jpg', 'image/jpeg', 'application/pdf', 'video/mp4'],
            LIMIT: 200 * 1000 * 1000
        }
    },
    RESPONSE_STATUS: {
        STATUS_SUCCESS: 'SUCCESS',
        STATUS_FAIL: 'FAIL'
    }
};
module.exports = {
    awsConfig: {
        "accessKeyId": process.env.AWS_ACCESS_ID,
        "secretAccessKey": process.env.AWS_SECRET_KEY,
        "region": process.env.AWS_REGION
    },
    emailConfig: {
        "sender": process.env.EMAIL_SENDER
    },
    smsConfig: {
        senderId: 'TEST',
        smsType: {
            Promotional: 'Promotional',
            Transactional: 'Transactional'
        }
    }
};
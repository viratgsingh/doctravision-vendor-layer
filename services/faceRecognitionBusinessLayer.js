/* eslint-disable max-statements */
var aws = require('aws-sdk');
const HttpStatus = require('http-status-codes');
var config = require('../config/config');
aws.config.update(config.awsConfig);

const indexFace = async (request) => {
    try {
        var imageParam = {};
        if (request.files && request.files.image) {
            imageParam.Bytes = request.files.image.data;
        } else {
            imageParam.S3Object = {};
            imageParam.S3Object.Bucket = process.env.AWS_BUCKET_NAME;
            imageParam.S3Object.Name = request.body.imageId;
        }
        var params = {
            CollectionId: request.body.collectionId,
            Image: imageParam,
            DetectionAttributes: [],
            ExternalImageId: request.body.externalId,
            MaxFaces: 1,
            QualityFilter: 'HIGH'
        };
        var awsS3Bucket = new aws.Rekognition();
        let data = await awsS3Bucket.indexFaces(params).promise();
        if (data.UnindexedFaces.length > 0) {
            throw new Error('Detected multiple faces. Please upload another image');
        }
        if (data.FaceRecords.length == 0) {
            throw new Error('Failed to detect any faces');
        }
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

/**
 * Detects faces in the image provided
 * @param {*} request 
 */
const detectFaces = async (request) => {
    try {
        var imageParam = {};
        if (request.files && request.files.image) {
            imageParam.Bytes = request.files.image.data;
        } else {
            imageParam.S3Object = {};
            imageParam.S3Object.Bucket = process.env.AWS_BUCKET_NAME;
            imageParam.S3Object.Name = request.body.imageId;
        }
        var params = {
            Image: imageParam,
            Attributes: []
        };
        var awsS3Bucket = new aws.Rekognition();
        let data = await awsS3Bucket.detectFaces(params).promise();
        if (data.FaceDetails.length > 1) {
            throw new Error('Detected multiple faces. Please upload another image');
        }
        if (data.FaceDetails.length == 0) {
            throw new Error('Failed to detect any faces');
        }
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const createCollection = async (collectionName) => {
    try {
        var params = {
            CollectionId: collectionName
        };
        var awsS3Bucket = new aws.Rekognition();
        let data = await awsS3Bucket.createCollection(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const deleteCollection = async (collectionName) => {
    try {
        var params = {
            CollectionId: collectionName
        };
        var awsS3Bucket = new aws.Rekognition();
        let data = await awsS3Bucket.deleteCollection(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const searchFaces = async (request) => {
    try {
        var imageParam = {};
        if (request.files && request.files.image) {
            imageParam.Bytes = request.files.image.data;
        } else {
            imageParam.S3Object = {};
            imageParam.S3Object.Bucket = process.env.AWS_BUCKET_NAME;
            imageParam.S3Object.Name = request.body.imageId;
        }
        var params = {
            CollectionId: request.body.collectionId,
            Image: imageParam,
            FaceMatchThreshold: 90,
            MaxFaces: 10,
            QualityFilter: 'HIGH'
        };
        var awsS3Bucket = new aws.Rekognition();
        let data = await awsS3Bucket.searchFacesByImage(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const listFaces = async (request) => {
    try {
        var params = {
            CollectionId: request.body.collectionId,
            MaxResults: 100
        };
        if (request.body.nextToken) {
            params.NextToken = request.body.nextToken;
        }
        var awsS3Bucket = new aws.Rekognition();
        let data = await awsS3Bucket.listFaces(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};


module.exports = {
    indexFace,
    createCollection,
    searchFaces,
    deleteCollection,
    detectFaces,
    listFaces
};
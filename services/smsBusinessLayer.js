var aws = require('aws-sdk');
const HttpStatus = require('http-status-codes');
const { RESPONSE_STATUS } = require('../config/constants');
var config = require('../config/config');
aws.config.update(config.awsConfig);

/**
 * Sends OTP SMS to the provided phone number
 * @param {*} phoneNumber 
 * @param {*} otp 
 */
const sendSMS = async (countryCode, phoneNumber, message) => {
    var awsSNS = new aws.SNS();
    var params = {
        Message: message,
        PhoneNumber: countryCode + phoneNumber,
        MessageAttributes: {
            'AWS.SNS.SMS.SenderID': {
                'DataType': 'String',
                'StringValue': config.smsConfig.senderId
            },
            'AWS.SNS.SMS.SMSType': {
                'DataType': 'String',
                'StringValue': config.smsConfig.smsType.Transactional
            }
        }
    };
    try {
        let response = await awsSNS.publish(params).promise();
        if (response.MessageId) {
            return {
                data: {
                    status: RESPONSE_STATUS.STATUS_SUCCESS,
                    message: 'Message Sent',
                    messageId: response.MessageId
                }
            };
        }
        throw new Error('Something went wrong');
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};


module.exports = {
    sendSMS
};
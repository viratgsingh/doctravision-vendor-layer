/* eslint-disable multiline-comment-style */
/* eslint-disable no-unused-vars */
/* eslint-disable max-statements */
var aws = require('aws-sdk');
const AdmZip = require('adm-zip');
const HttpStatus = require('http-status-codes');
var config = require('../config/config');
const uuid = require('uuid');
aws.config.update(config.awsConfig);

const uploadFile = async (request) => {
    try {
        let file = request.files.file;
        var params = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Key: `${request.body.folder}/${uuid.v4()}` + '.' + file.name.split('.').pop(),
            Body: file.data,
            ContentType: file.mimetype,
            ACL: "public-read"
        };
        var awsS3Bucket = new aws.S3();
        let data = await awsS3Bucket.upload(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const getFile = async (fileKey) => {
    try {
        var params = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Key: `${fileKey}`
        };
        var awsS3Bucket = new aws.S3();
        var data = await awsS3Bucket.getObject(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const listFiles = async (folder) => {
    try {
        var params = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Prefix: folder + '/',
        };
        var awsS3Bucket = new aws.S3();
        var data = await awsS3Bucket.listObjects(params).promise();
        return data;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};

const downloadFiles = async (files) => {
    try {
        var awsS3Bucket = new aws.S3();
        const zip = new AdmZip(); 
        await Promise.all(files.map(async (file) => {
            var filePath = file.url.split(awsS3Bucket.endpoint.host + '/')[1];
            var params = {
                Bucket: process.env.AWS_BUCKET_NAME,
                Key: `${filePath}`
            };
            try {
                var data = await awsS3Bucket.getObject(params).promise();
                if (data.Body) {
                    zip.addFile(file.name, data.Body);
                }
            } catch (error) {
                console.log(error.message);
            }
        }));
        // await archive.finalize();
        return zip;
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};


module.exports = {
    uploadFile,
    getFile,
    downloadFiles,
    listFiles
};
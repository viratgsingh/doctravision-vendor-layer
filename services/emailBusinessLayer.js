var aws = require('aws-sdk');
const HttpStatus = require('http-status-codes');
const { RESPONSE_STATUS } = require('../config/constants');
var config = require('../config/config');
aws.config.update(config.awsConfig);

const buildEmail = (recipients, subject, emailBody, isHtml = false) => {
    var params = {
        Source: config.emailConfig.sender,
        Destination: {
            ToAddresses: recipients
        },
        Message: {
            Subject: {
                Data: subject,
                Charset: 'UTF-8'
            },
            Body: {}
        },
        // ConfigurationSetName: configuration_set
    };
    if (isHtml) {
        params.Message.Body.Html = {
            Data: emailBody,
            Charset: 'UTF-8'
        };
    } else {
        params.Message.Body.Text = {
            Data: emailBody,
            Charset: 'UTF-8'
        };
    }
    return params;
};

/**
 * Sends email to the provided email address
 * @param {*} recipient 
 * @param {*} emailBody 
 */
const sendEmail = async (recipient, subject, emailBody, isHtml = false) => {
    var awsSES = new aws.SES();
    const emailParams = buildEmail([recipient], subject, emailBody, isHtml);
    try {
        let response = await awsSES.sendEmail(emailParams).promise();
        if (response.MessageId) {
            return {
                data: {
                    status: RESPONSE_STATUS.STATUS_SUCCESS,
                    message: 'Email Sent',
                    messageId: response.MessageId
                }
            };
        }
        throw new Error('Something went wrong');
    } catch (error) {
        return {
            error: {
                errorCode: (error.statusCode || HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                message: error.message || 'Something went wrong'
            }
        };
    }
};


module.exports = {
    sendEmail
};
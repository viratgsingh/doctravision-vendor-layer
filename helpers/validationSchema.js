/* eslint-disable id-length */
const joi = require("joi");
const { FILE_UPLOAD_CONFIG } = require('../config/constants');

const sendEmailSchema = joi.object({
    email: joi.string().email().lowercase().required(),
    subject: joi.string().required(),
    body: joi.string().required(),
    isHtml: joi.boolean().default(false).optional()
});
const sendSMSSchema = joi.object({
    countryCode: joi.string().min(1).max(4).required(),
    phone: joi.string().min(5).max(15).required(),
    message: joi.string().required()
});

const fileUploadSchema = joi.object({
    folder: joi.string().required(),
    file: joi.object().keys({
        mimetype: joi.string().required(),
        size: joi.number().required().error((size) => {
            return new Error(`File size cannot be more than ${(size[0].local.limit / 1000000)} MB`);
        })
    }).required()
});

const fileGetSchema = joi.object().keys({
    file: joi.string().required()
});

const listFilesSchema = joi.object({
    folder: joi.string().required()
});

const fileDownloadSchema = joi.object({
    files: joi.array().min(1).required(),
    folder: joi.string().optional(),
});

const indexFaceSchema = joi.object({
    collectionId: joi.string().required(),
    externalId: joi.string().required(),
    imageId: joi.string().optional(),
    image: joi.object().keys({
        mimetype: joi.string().valid(...FILE_UPLOAD_CONFIG.IMAGE.MIME_TYPES).required(),
        size: joi.number().less(FILE_UPLOAD_CONFIG.IMAGE.LIMIT).required().error((size) => {
            return new Error(`Image size cannot be more than ${(size[0].local.limit / 1000000)} MB`);
        })
    }).when('imageId', {
        is: joi.exist(),
        then: joi.optional(),
        otherwise: joi.required()
    })
});

const detectFacesSchema = joi.object({
    imageId: joi.string().optional(),
    image: joi.object().keys({
        mimetype: joi.string().valid(...FILE_UPLOAD_CONFIG.IMAGE.MIME_TYPES).required(),
        size: joi.number().less(FILE_UPLOAD_CONFIG.IMAGE.LIMIT).required().error((size) => {
            return new Error(`Image size cannot be more than ${(size[0].local.limit / 1000000)} MB`);
        })
    }).when('imageId', {
        is: joi.exist(),
        then: joi.optional(),
        otherwise: joi.required()
    })
});

const collectionIdSchema = joi.object({
    collectionId: joi.string().required()
});

const searchFacesSchema = joi.object({
    collectionId: joi.string().required(),
    imageId: joi.string().optional(),
    image: joi.object().keys({
        mimetype: joi.string().valid(...FILE_UPLOAD_CONFIG.IMAGE.MIME_TYPES).required(),
        size: joi.number().less(FILE_UPLOAD_CONFIG.IMAGE.LIMIT).required().error((size) => {
            return new Error(`Image size cannot be more than ${(size[0].local.limit / 1000000)} MB`);
        })
    }).when('imageId', {
        is: joi.exist(),
        then: joi.optional(),
        otherwise: joi.required()
    })
});

const listFacesSchema = joi.object({
    collectionId: joi.string().required(),
    nextToken: joi.string().optional()
});

module.exports = {
    sendEmailSchema,
    sendSMSSchema,
    fileUploadSchema,
    fileGetSchema,
    indexFaceSchema,
    collectionIdSchema,
    searchFacesSchema,
    detectFacesSchema,
    listFacesSchema,
    fileDownloadSchema,
    listFilesSchema
};
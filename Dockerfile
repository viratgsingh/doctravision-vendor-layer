FROM node:lts-alpine3.10
WORKDIR /cfu-vendor
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent && mv node_modules ../cfu-vendor
COPY . /cfu-vendor
EXPOSE 8002
RUN npm install pm2 -g
ENV PM2_PUBLIC_KEY wmp19bpm94r3htq
ENV PM2_SECRET_KEY 4436hllu2z8nuad

CMD ["pm2-runtime", "index.js", "--name", "cfu-vendor", "--output", "/var/log/cfu-vendor/cfu-vendor-output.log", "--error", "/var/log/cfu-vendor/cfu-vendor-error.log"]

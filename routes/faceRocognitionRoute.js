const router = require('express').Router();
const faceRecognitionController = require("../controllers/faceRecognitionController");

router.post('/indexFace', faceRecognitionController.indexFace);

router.post('/detectFaces', faceRecognitionController.detectFaces);

router.get('/createCollection', faceRecognitionController.createCollection);

router.get('/deleteCollection', faceRecognitionController.deleteCollection);

router.post('/searchFaces', faceRecognitionController.searchFaces);

router.post('/listFaces', faceRecognitionController.listFaces);

module.exports = router;
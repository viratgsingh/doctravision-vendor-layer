const router = require('express').Router();
const fileController = require("../controllers/fileController");

router.post('/uploadFile', fileController.uploadFile);

router.get('/getFile/:file*', fileController.getFile);

router.get('/listFiles/:folder*', fileController.listFiles);

router.post('/downloadFiles', fileController.downloadFiles);

module.exports = router;
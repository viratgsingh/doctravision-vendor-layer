const router = require('express').Router();
const smsController = require("../controllers/smsController");

router.post('/send', smsController.sendOTP);

module.exports = router;